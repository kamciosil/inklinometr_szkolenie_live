package com.example.alexxa.pomiarwychylenia;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Gravity rotational data
    private float gravity[];
    // Magnetic rotational data
    private float magnetic[]; //for magnetic rotational data
    private float accels[] = new float[3];
    private float mags[] = new float[3];
    private float[] values = new float[3];

    // azimuth, pitch and roll
    private float azimuth;
    private float pitch;
    private float roll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //pobiera layout z naszej

        final TextView azimuthTextView = findViewById(R.id.azimuthValTextView); //resource
        final TextView pitchTextView = findViewById(R.id.pitchValTextView);
        final TextView rollTextView = findViewById(R.id.rollValTextView);

        Button updateTextButton = findViewById(R.id.updateValButton); //tworzymy zmienna
        updateTextButton.setOnClickListener(new View.OnClickListener() {    //na klikniecie
            @Override //przeciaznie czyli mowi co ma zrobic na onClick zamiast swojego zdefiniowanego w klasie
            public void onClick(View view) {
               // azimuthTextView.setText("klik"); //co on ma robic na klikniecie
//                azimuthTextView.setText(String.valueOf(azimuthTextView));
//                pitchTextView.setText(String.valueOf(pitch));
//                rollTextView.setText(String.valueOf(roll));
            }
        });

        SensorManager sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE); //dostep do sensorow
        SensorEventListener sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                switch (event.sensor.getType()) {
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        mags = event.values.clone();
                        break;
                    case Sensor.TYPE_ACCELEROMETER:
                        accels = event.values.clone();
                        break;
                }

                if (mags != null && accels != null && azimuthTextView !=null && pitchTextView !=null && rollTextView !=null) {
                    gravity = new float[9];
                    magnetic = new float[9];
                    SensorManager.getRotationMatrix(gravity, magnetic, accels, mags);
                    float[] outGravity = new float[9];
                    SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_X, SensorManager.AXIS_Z, outGravity);
                    SensorManager.getOrientation(outGravity, values);
                    azimuth = values[0] * 57.2957795f;
                    pitch = values[1] * 57.2957795f;
                    roll = values[2] * 57.2957795f;
                    mags = null;
                    accels = null;

                    azimuthTextView.setText(String.valueOf(azimuth));
                    pitchTextView.setText(String.valueOf(pitch));
                    rollTextView.setText(String.valueOf(roll));
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);

    }
}
